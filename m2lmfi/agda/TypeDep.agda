module TypeDep where

-- Rappel: le type vide représentant la proposition False

data False : Set where

-- Rappel: l'egalité de Leibniz

data _≡_ {A : Set} (x : A) : A → Set where
  refl : x ≡ x

infix 4 _≡_

-- Rappel: les entiers et l'addition

data Nat : Set where
 O : Nat
 S : Nat -> Nat

{-# BUILTIN NATURAL Nat #-}

_+_ : Nat -> Nat -> Nat
O + m = m
S n + m = S (n + m)

infixl 6 _+_

-- Pour comparer, une structure de donnée non-dépendante : les listes

data List (A : Set) : Set where
  Nil : List A
  Cons : A -> List A -> List A

exampleList : List Nat
exampleList = Cons 3 (Cons 5 Nil)

-- Type dependant : les vecteurs de taille n

data Vec (A : Set) : Nat -> Set where
 VNil : Vec A O
 VCons : {n : Nat} -> A -> Vec A n -> Vec A (S n)

exampleVec : Vec Nat 2
exampleVec = VCons 3 (VCons 5 VNil)

head : forall {n A} ->  Vec A (S n) -> A
head (VCons x _) = x

-- Interet: head VNil est exclu par typage !

app : forall {n m A} -> Vec A n -> Vec A m -> Vec A (n + m)
app VNil v = v
app (VCons x v1) v2 = VCons x (app v1 v2)

-- Inconvenient: Vec A (n+1) n'est pas la meme chose que Vec A (1+n)
-- donc la definition suivante de rev est refusee

--rev : forall {n A} -> Vec A n -> Vec A n
--rev VNil = VNil
--rev (VCons x v) = app (rev v) (VCons x VNil)

-- Il faut une coercion entre ces types, soit manuelle:

coerc_one : forall {n A} -> Vec A (n + 1) -> Vec A (S n)
coerc_one {O} v = v
coerc_one {S n} (VCons x v) = VCons x (coerc_one v)

rev : forall {n A} -> Vec A n -> Vec A n
rev VNil = VNil
rev (VCons x v) = coerc_one (app (rev v) (VCons x VNil))

-- Soit une coercion generale basee sur l'egalite:

coerc : forall {n m A} -> Vec A n -> n ≡ m -> Vec A m
coerc v refl = v

cong : {A B : Set} (f : A -> B) {x y : A} -> x ≡ y -> f x ≡ f y
cong f refl = refl

plusone : forall {n} -> n + 1 ≡ S n
plusone {O} = refl
plusone {S n} = cong S plusone

rev' : forall {n A} -> Vec A n -> Vec A n
rev' VNil = VNil
rev' (VCons x v) = coerc (app (rev' v) (VCons x VNil)) plusone



-- Autre type dependant : des ensembles à cardinal donné
-- (Fin n) est un type à exactement n element. 
-- (Fin n) peut aussi être vu comme le type des entiers naturels < n

data Fin : Nat → Set where
  zero : {n : _} → Fin (S n)
  suc  : {n : _} → Fin n → Fin (S n)

-- Fin 0 est vide : on a ex-falso-quodlibet, tout comme pour False

noFin0 : {A : Set} -> Fin 0 -> A
noFin0 ()

uniqFin1 : Fin 1
uniqFin1 = zero

Fin1Singleton : (n m : Fin 1) -> n ≡ m
Fin1Singleton zero zero = refl
Fin1Singleton (suc ()) _
Fin1Singleton _ (suc ())

firstOf3 : Fin 3
firstOf3 = zero

secondOf3 : Fin 3
secondOf3 = suc zero

thirdOf3 : Fin 3
thirdOf3 = suc (suc zero)

-- Utilisation de Fin : acces garanti dans les bornes d'un vecteur

nth : forall {A n} -> Vec A n -> Fin n -> A
nth (VCons a _) zero = a
nth (VCons _ l) (suc m) = nth l m

-- Conversion List<->Vec

Vec2List : forall {A n} -> Vec A n -> List A
Vec2List VNil = Nil
Vec2List (VCons x l) = Cons x (Vec2List l)

len : forall {A} -> List A -> Nat
len Nil = O
len (Cons _ l) = S (len l)

List2Vec : forall {A} -> (l : List A) -> Vec A (len l)
List2Vec Nil = VNil
List2Vec (Cons a l) = VCons a (List2Vec l)
