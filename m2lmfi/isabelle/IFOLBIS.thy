theory IFOLBIS
imports Pure
begin

class "term"
default_sort "term" (* restrict all type variables to sort "term" *)

typedecl o

judgment
  Trueprop      :: "o => prop"                  ("(_)" 5)

axiomatization
  eq :: "['a, 'a] => o"  (infixl "=" 50)
where
  refl:         "a=a" and
  subst:        "a=b \<Longrightarrow> P(a) \<Longrightarrow> P(b)"

axiomatization
  False :: o and
  conj :: "[o, o] => o"  (infixr "&" 35) and
  disj :: "[o, o] => o"  (infixr "|" 30) and
  imp :: "[o, o] => o"  (infixr "-->" 25)
where
  conjI: "[| P;  Q |] ==> P&Q" and
  conjunct1: "P&Q ==> P" and
  conjunct2: "P&Q ==> Q" and

  disjI1: "P ==> P|Q" and
  disjI2: "Q ==> P|Q" and
  disjE: "[| P|Q;  P ==> R;  Q ==> R |] ==> R" and

  impI: "(P ==> Q) ==> P-->Q" and
  mp: "[| P-->Q;  P |] ==> Q" and

  FalseE: "False ==> P"

(* lemma test : "False = False"
   refus car False est un o, incompatible avec un 'a dans term
*)

axiomatization
  All :: "('a => o) => o"  (binder "ALL " 10) and
  Ex :: "('a => o) => o"  (binder "EX " 10)
where
  allI: "(!!x. P(x)) ==> (ALL x. P(x))" and
  spec: "(ALL x. P(x)) ==> P(x)" and
  exI: "P(x) ==> (EX x. P(x))" and
  exE: "[| EX x. P(x);  !!x. P(x) ==> R |] ==> R"

lemma conjE:
  assumes major: "P & Q"
    and r: "[| P; Q |] ==> R"
  shows R
  apply (rule r)
  apply (rule conjunct1)
   apply (rule major)
  apply (rule conjunct2)
   apply (rule major)
  done

lemma impE:
  assumes major: "P --> Q"
    and p:P
  and r: "Q ==> R"
  shows R
  apply (rule r)
  apply (rule mp)
  apply (rule major)
  apply (rule p) (* or (rule `P`) *)
  done

(* lemma test : "ALL P. P --> P" (* pas de quantification sur les propositions *) *)
