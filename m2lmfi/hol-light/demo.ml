(** Rules of HOL Light : https://en.wikipedia.org/wiki/HOL_Light *)

(** See also Hol-light Tutorial :
    http://www.cl.cam.ac.uk/~jrh13/hol-light/tutorial_220.pdf
*)

#use "hol.ml";;
(* or use a saved snapshot, for instance via dmtcp: ./dmtcp_restart_script.sh *)

(o);;

`x+1`;;

#remove_printer pp_print_qterm;;

`x+10`;;

#install_printer pp_print_qterm;;

REFL `x:num`;;

ARITH_RULE
`(a * x + b * y + a * y) EXP 3 + (b * x) EXP 3 +
(a * x + b * y + b * x) EXP 3 + (a * y) EXP 3 =
(a * x + a * y + b * x) EXP 3 + (b * y) EXP 3 +
(a * y + b * y + b * x) EXP 3 + (a * x) EXP 3`;;

TAUT `p \/ ~p ==> F <=> ~T`;;

infixes();;

`(x:'a)=y ==> y=x`;;

(** preuves en style "pedestre" *)

let sym1 =
    MK_COMB (REFL `(=):'a->'a->bool`,
             ASSUME `(x:'a)=y`);;
let sym2 = MK_COMB (sym1, REFL `x:'a`);;
let sym3 = EQ_MP sym2 (REFL `x:'a`);;

let full_sym =
  EQ_MP (MK_COMB (MK_COMB (REFL `(=):'a->'a->bool`,
                           ASSUME `(x:'a)=y`),
                  REFL `x:'a`))
        (REFL `x:'a`);;

(** Et la transitivité (règle de base, mais "superflue"...) *)

let trans =
   EQ_MP
     (MK_COMB (REFL `(=) (x:'a)`, ASSUME `(y:'a)=z`))
     (ASSUME `(x:'a)=y`);;

TRANS (ASSUME `(x:'a)=y`) (ASSUME `(y:'a)=z`);;

(** version abstraite de la symetrie *)

let SYM th =
  let tm = concl th in
  let l,r = dest_eq tm in
  let eq = rator (rator tm) in
  let lth = REFL l in
  EQ_MP (MK_COMB(MK_COMB (REFL eq,th),lth)) lth;;

let sym_again = SYM (ASSUME `(x:'a)=y`);;

(** preuves "interactives" *)

g `(x:'a)=y ==> y=x`;;

e DISCH_TAC;;
e (ASM_REWRITE_TAC[]);;
b () (* backtrack in the proof *)
e (CLAIM_TAC "helper" `((x:'a)=x) <=> (y=x)`);;
e MK_COMB_TAC;;
e MK_COMB_TAC;;
e REFL_TAC;;
e (FIRST_ASSUM ACCEPT_TAC);;
e REFL_TAC;;
e (FIRST_ASSUM (fun h -> ACCEPT_TAC (EQ_MP h (REFL `x:'a`))));;
(*ou:
(USE_THEN "helper" (fun h -> ACCEPT_TAC (EQ_MP h (REFL `x:'a`))));;
*)

let the_sym = top_thm();;

g `(x:'a)=y ==> y=x`;;
e (DISCH_TAC THEN
   CLAIM_TAC "helper" `((x:'a)=x) <=> (y=x)`
   THENL
   [ MK_COMB_TAC THENL
     [ MK_COMB_TAC THENL
       [REFL_TAC;
        FIRST_ASSUM ACCEPT_TAC];
     REFL_TAC ];
   FIRST_ASSUM (fun h -> ACCEPT_TAC (EQ_MP h (REFL `x:'a`)))]);;

let sym_again = prove
  (`(x:'a)=y ==> y=x`,
    DISCH_TAC THEN
   CLAIM_TAC "helper" `((x:'a)=x) <=> (y=x)`
   THENL
   [ MK_COMB_TAC THENL
     [ MK_COMB_TAC THENL
       [REFL_TAC;
        FIRST_ASSUM ACCEPT_TAC];
     REFL_TAC ];
   FIRST_ASSUM (fun h -> ACCEPT_TAC (EQ_MP h (REFL `x:'a`)))]);;

(** Attention! la cohérence d'HOL-Light repose de manière essentielle
    sur les propriétés du typage d'OCaml, et en particulier sur le
    fait qu'on ne peut pas fabriquer de valeur du type 'thm' (type
    qui est abstrait) sans utiliser les constructeurs fournis (REFL,
    ASSUME, ...). Sauf qu'en pratique OCaml fournit aussi (de manière
    non documenté et non recommandé) des fonctions qui "trichent"
    avec le typage. Et on peut donc s'en servir pour obtenir une
    preuve de faux en HOL-Light... *)

let oups : thm = Obj.magic [| ([],`F`) |];;

(** Attention, tous les domaines sont considérés non-vides, donc
    `!x.A = A` dès que A ne contient pas de x.
    Ceci n'est pas vrai en général en Coq, par exemple
    (forall x:False, False) = False->False, qui est équivalent
    à True, et non à False.
    Cf. aussi l'énoncé HOL du lemme des buveurs, qui n'a pas
    besoin d'expliciter que le bar est non-vide, avant d'affirmer
    qu'il existe quelqu'un dans le bar, qui s'il boit, alors
    tout le monde boit. *)

TAUT `(!x.F) = F`;;
