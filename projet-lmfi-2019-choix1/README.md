LMFI : Projet de Preuves 2019
=============================

Marthe : un mini-compilateur certifié. Vous trouverez des instructions plus détaillées en commentaire dans le fichier à compléter [`Marthe.v`](Marthe.v).

Travail réalisable par groupe d'au plus deux étudiants.

Date de rendu : à préciser, autour de la période d'examen.

Listes de tactiques utiles pour le projet
-----------------------------------------

- `revert` préalablement à certaines utilisations de `induction` ;
- `eauto` ainsi que sa variante `eauto using` ; vous pouvez également ajouter des indices pour `eauto` à l'aide de la commande `Hint Resolve` ;
- `eapply` peut être également utile notamment pour appliquer les lemmes de transitivité.
