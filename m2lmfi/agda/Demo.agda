module Demo where

-- NB: Attention aux conventions syntaxiques d'agda:
-- A part ( ) et { } tout peut faire parti d'un nom d'identifieur
-- Par exemple 1+2x représente un identifieur valide
-- qui diffère de 1 + 2 * x.
-- Bref, il faut des blancs presque partout !!

-- NB: Attention aussi aux symboles unicodes
-- (p.ex. lors de copier-coller depuis le web) :
-- * et ∗ ne sont pas les mêmes caractères !!


-- Les entiers : definitions

data Nat : Set where
 O : Nat
 S : Nat -> Nat

_+_ : Nat -> Nat -> Nat
O + m = m
S n + m = S (n + m)

_*_ : Nat -> Nat -> Nat
O * m = O
S n * m = m + (n * m)

infixl 6 _+_
infixl 8 _*_

-- Pour utiliser les entiers habituels 0 1 2 3 4 ...
{-# BUILTIN NATURAL Nat #-}

example : Nat
example = 1 + 2 * 3

-- Un peu de logique...

data True : Set where
 tt : True

data False : Set where  -- pas de constructeur, False est donc vide

FalseElim : {A : Set} -> False -> A
FalseElim ()
-- ce () est le pattern vide, impossible (donc sans code à droite)
-- on peut aussi ecrire : FalseElim = λ ()

data And (A B : Set) : Set where
 conj : A -> B -> And A B

data Or (A B : Set) : Set where
 left : A -> Or A B
 right : B -> Or A B

OrElim : {A B C : Set} -> Or A B -> (A -> C) -> (B -> C) -> C
OrElim (left a) ac bc = ac a
OrElim (right b) ac bc = bc b

data Exists (A : Set) (P : A -> Set) : Set where
 exintro : (x : A) -> P x -> Exists A P

-- On a plusieurs possibilités pour énoncer des égalités d'entiers
-- (et en faire les preuves)

module LeibnizEq where

  -- Une définition générique de l'égalité (dite de Leibniz)

  data _≡_ {A : Set} (x : A) : A → Set where
    refl : x ≡ x

  infix 4 _≡_

  what-is-example : example ≡ 7
  what-is-example = refl

  sym : forall {A} -> {x y : A} -> x ≡ y -> y ≡ x
  sym refl = refl

  trans : forall {A} -> {x y z : A} -> x ≡ y -> y ≡ z -> x ≡ z
  trans refl refl = refl

  cong : {A B : Set} (f : A -> B) {x y : A} -> x ≡ y -> f x ≡ f y
  cong f refl = refl

  eqS : {n m : Nat} -> n ≡ m -> S n ≡ S m
  eqS = cong S

  injS : forall {n m} -> S n ≡ S m -> n ≡ m
  injS refl = refl

  plusO : forall n -> n + O ≡ n
  plusO O = refl
  plusO (S n) = eqS (plusO n)

  plusS : forall n m -> n + S m ≡ S (n + m)
  plusS O m = refl
  plusS (S n) m = eqS (plusS n m)

  -- NB: pour pluscom, c'est mieux d'analyser m,
  -- ça nous amène exactement à plusO et plusS.
  -- Si on analyse n, il faudra insérer des sym.

  pluscom : forall n m -> n + m ≡ m + n
  pluscom n O = plusO n
  pluscom n (S m) = trans (plusS n m) (eqS (pluscom n m))

  plusassoc : forall n m p -> n + (m + p) ≡ n + m + p
  plusassoc O m p = refl
  plusassoc (S n) m p = eqS (plusassoc n m p)

module ComputeEq where

  -- Autre possibilité: une égalité définie comme une
  -- fonction répondant True ou False.
  -- Interet : des calculs dans les goals
  -- Souci : les arguments implicites marchent mal (cf Refl...)

  infix 4 _==_

  _==_ : Nat -> Nat -> Set
  O == O = True
  S n == S m = n == m
  _ == _ = False

  what-is-example : example == 7
  what-is-example = tt

  refl : {n : Nat} -> n == n
  refl {O} = tt
  refl {S n} = refl {n}

  sym : {n m : Nat} -> n == m -> m == n
  sym {O} {O} tt = tt
  sym {S n} {S m} h = sym {n} {m} h
  sym {O} {S _} h = h
  sym {S _} {O} h = h

  trans : {n m p : Nat} -> n == m -> m == p -> n == p
  trans {O}   {O}   {_} h g = g
  trans {O}   {S _} {_} h g = FalseElim h
  trans {S _} {O}   {_} h g = FalseElim h
  trans {S _} {S _} {O} h g = FalseElim g
  trans {S n} {S m} {S p} h g = trans {n} {m} {p} h g

  plusO : forall n -> n + O == n
  plusO O = tt
  plusO (S n) = plusO n

  plusS : forall n m -> n + S m == S (n + m)
  plusS O m = refl {m}
  plusS (S n) m = plusS n m

  pluscom : forall n m -> n + m == m + n
  pluscom n O = plusO n
  pluscom n (S m) =
    trans {n + S m} {_} {_}
      (plusS n m)
      (pluscom n m)

  plusassoc : forall n m p -> n + (m + p) == n + m + p
  plusassoc O m p = refl {m + p}
  plusassoc (S n) m p = plusassoc n m p

  -- Preuves d'equivalence avec Leibniz

  open LeibnizEq using (_≡_) renaming (refl to leib-refl; eqS to leib-eqS)

  toLeibniz : {n m : Nat} -> n == m -> n ≡ m
  toLeibniz {O} {O} h = leib-refl
  toLeibniz {S n} {S m} h = leib-eqS (toLeibniz h)
  toLeibniz {O} {S _} h = FalseElim h
  toLeibniz {S _} {O} h = FalseElim h

  fromLeibniz : {n m : Nat} -> n ≡ m -> n == m
  fromLeibniz {n} leib-refl = refl {n}

module IndEq where

  -- Une dernière possibilité à la mode actuellement:
  -- l'égalité sur les entiers vue comme prédicat inductif
  -- spécialisé aux entiers.
  -- Fonctionne de façon similaire à Leibniz

  infix 4 _==_

  data _==_ : Nat -> Nat -> Set where
    eqO : O == O
    eqS : forall {n m} -> n == m -> S n == S m

  what-is-example : example == 7
  what-is-example = eqS (eqS (eqS (eqS (eqS (eqS (eqS eqO))))))

  refl : {n : Nat} -> n == n
  refl {O} = eqO
  refl {S n} = eqS refl

  what-is-example-bis : example == 7
  what-is-example-bis = refl

  injS : forall { n m } -> S n == S m -> n == m
  injS (eqS h) = h

  sym : {n m : Nat} -> n == m -> m == n
  sym eqO = eqO
  sym (eqS h) = eqS (sym h)

  trans : {n m p : Nat} -> n == m -> m == p -> n == p
  trans eqO eqO = eqO
  trans (eqS h) (eqS g) = eqS (trans h g)

  plusO : forall n -> n + O == n
  plusO O = eqO
  plusO (S n) = eqS (plusO n)

  plusS : forall n m -> n + S m == S (n + m)
  plusS O m = refl
  plusS (S n) m = eqS (plusS n m)

  pluscom : forall n m -> n + m == m + n
  pluscom n O = plusO n
  pluscom n (S m) = trans (plusS n m) (eqS (pluscom n m))

  plusassoc : forall n m p -> n + (m + p) == n + m + p
  plusassoc O m p = refl
  plusassoc (S n) m p = eqS (plusassoc n m p)

  -- Preuves d'equivalence avec Leibniz

  open LeibnizEq using (_≡_) renaming (refl to leib-refl; eqS to leib-eqS)

  toLeibniz : {n m : Nat} -> n == m -> n ≡ m
  toLeibniz eqO = leib-refl
  toLeibniz (eqS h) = leib-eqS (toLeibniz h)

  fromLeibniz : {n m : Nat} -> n ≡ m -> n == m
  fromLeibniz leib-refl = refl

-- NB: pour les preuves automatisables, il y a un système
-- de "reflection" (meta-programming), qui se rapproche fortement
-- des tactiques de Coq !!
-- Voir:
--  https://agda.readthedocs.io/en/v2.5.4.1/language/reflection.html
--  https://oisdk.github.io/agda-ring-solver/README.html
