{
  open Lexing
}

let newline = ('\010' | '\013' | "\013\010")

let blank   = [' ' '\009' '\012']

let digit = ['0'-'9']

let alpha = ['a'-'z' 'A'-'Z' '_']

let alphanum = alpha | digit | '_'

let identifier = alpha alphanum*

rule get_token = parse
  (** Layout *)
  | newline         { get_token lexbuf }
  | blank+          { get_token lexbuf               }

  | "True"          { TRUE }
  | "False"         { FALSE }
  | "/\\"           { AND  }
  | "\\/"           { OR   }
  | "<->"           { IFF  }
  | "->"            { IMPL }
  | "∀"             { ALL  }
  | "∃"             { EX   }
  | "~"             { NOT }
  | "("             { LPAREN }
  | ")"             { RPAREN }
  | "{"             { LCURL }
  | "}"             { RCURL }
  | ","             { COMMA }

  | "="             { EQ }
  | "∈"             { IN }
  | "+"             { PLUS }
  | "*"             { MULT }

  | digit+ as d     { INT (int_of_string d) }

  | identifier as i { ID i }

  | eof             { EOF       }

  | _               { failwith "unexpected character" }
