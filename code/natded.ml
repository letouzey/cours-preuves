
(** * Natded : a toy implementation of Natural Deduction *)

(** Pierre Letouzey, © 2018-today *)

(** A signature : a list of function symbols (with their arity)
    and a list of predicate symbols (with their arity).
    Functions of arity zero are also called constants.

    Note : in theory a signature could be infinite and hence not
    representable by some lists, but we'll never do this in practice.
*)

type function_symbol = string
type predicate_symbol = string
type arity = int

type signature =
  { fun_symbs : (function_symbol * arity) list;
    pred_symbs : (predicate_symbol * arity) list }

(** In pratice, the symbols could be special characters as "+", or
    names. In fact, pretty much anything that doesn't contain
    the parenthesis characters or the comma. *)

let peano_sign =
  { fun_symbs = [("O",0);("S",1);("+",2);("*",2)];
    pred_symbs = [("=",2)] }

let zf_sign =
  { fun_symbs = [];
    pred_symbs = [("=",2);("∈",2)] }

(** Variables are coded as string, and will follow the usual
    syntactic conventions for identifiers : a letter first, then
    letters or digits or "_". *)

type variable = string

(** Instead of list of variables, we'll use sets of variables
    (faster search) *)

module VarSet = Set.Make (struct type t = variable  let compare = compare end)

(** [fresh_var vars] : gives a new variable not in the set [vars]. *)

let fresh_var vars =
  let rec loop i =
    let v = "x"^string_of_int i in
    if VarSet.mem v vars then loop (i+1) else v
  in loop 0

(** A term is given by the following recursive definition: *)

type term =
  | Var of variable
  | Fun of function_symbol * term list

let peano_term_example =
  Fun ("+",[Fun ("S",[Fun ("O",[])]); Var "x"])

(** In the case of Peano, numbers are coded as iterated successors of zero *)

let rec int2term n =
 if n = 0 then Fun ("O",[])
 else Fun ("S",[int2term (n-1)])

let option_map f = function
  | None -> None
  | Some x -> Some (f x)

let rec term2int = function
  | Fun ("O",[]) -> Some 0
  | Fun ("S",[t]) -> option_map succ (term2int t)
  | _ -> None

(** Term printing

    NB: + and * are printed in infix position, S(S(...O())) is printed as
    the corresponding number.
*)

let print_tuple pr l = "(" ^ String.concat "," (List.map pr l) ^ ")"

let rec print_term t =
  match term2int t with
  | Some n -> string_of_int n
  | None ->
     match t with
     | Var v -> v
     | Fun ("+"|"*" as op,[t1;t2]) -> print_terms [t1] ^ op ^ print_terms [t2]
     | Fun (f,args) -> f ^ print_terms args

and print_terms l = print_tuple print_term l

let _ = print_term peano_term_example

(** Term parsing *)

(** Actually, parsing is not so easy and not so important.
    Let's put the details elsewhere, and take for granted that
    parsing is doable :-).
*)

(* See formula parsing below... *)

(** With respect to a particular signature, a term is valid
    iff it only refer to known function symbols and use them
    with the correct arity. *)

let rec check_term sign = function
  | Var _ -> true
  | Fun (f,args) ->
     match List.assoc f sign.fun_symbs with
     | exception Not_found -> false
     | arity when List.length args <> arity -> false
     | _ -> List.for_all (check_term sign) args

let _ = check_term peano_sign peano_term_example

(** The set of variables occurring in a term *)

let rec term_vars = function
  | Var v -> VarSet.singleton v
  | Fun (_,args) -> termlist_vars args

and termlist_vars = function
  | [] -> VarSet.empty
  | t::ts -> VarSet.union (term_vars t) (termlist_vars ts)

(** Substitution of a variable in a term :
    in [t], [v] is replaced by [u] *)

let rec term_subst (v,u) t = match t with
  | Var v' -> if v=v' then u else t
  | Fun (f,args) -> Fun (f, List.map (term_subst (v,u)) args)


(** Formulas *)

type op = And | Or | Impl

type quant = All | Ex

type formula =
  | True
  | False
  | Pred of predicate_symbol * term list
  | Not of formula
  | Op of op * formula * formula
  | Quant of quant * variable * formula

(** One extra pseudo-constructor :
    [a<->b] is a shortcut for [a->b /\ b->a] *)

let iff a b = Op(And,Op(Impl,a,b),Op(Impl,b,a))

(** Formula printing *)

(** Notes:
    - We use {  } for putting formulas into parenthesis, instead of ( ).
    - The unicode ∀ and ∃ are not displayed correctly in strings by the OCaml
      toplevel, do a print_string on these strings to display them correctly.
    - /\ and \/ trigger Ocaml's warning 14, but we don't care (menhir's
      parser works ok).
*)

let pr_op = function
  | And -> "/\\"
  | Or -> "\\/"
  | Impl -> "->"

let pr_quant = function All -> "∀" | Ex -> "∃"

let rec print_formula = function
  | True -> "True"
  | False -> "False"
  | Pred ("="|"∈" as p,[t1;t2]) -> print_terms [t1] ^ p ^ print_terms [t2]
  | Pred (p,args) -> p ^ print_terms args
  | Not f -> "~{" ^ print_formula f ^"}"
  | Op(And,Op(Impl,a,b),Op(Impl,b',a')) when a=a' && b=b' ->
    "{" ^ print_formula a ^ "}<->{" ^ print_formula b ^"}"
  | Op (o,f,f') ->
    "{" ^ print_formula f ^ "}"^pr_op o^"{" ^ print_formula f' ^"}"
  | Quant(q,v,f) -> pr_quant q ^ v ^ ",{"^print_formula f^"}"

let _ = print_string (print_formula (Quant(Ex,"x",True)))

let _ = print_formula (iff True False);;

(** Formula parsing *)

(** Just as for term parsing, let's skip the parsing code,
    and simply accept it is doable. Priority and conventions are
    inspired by Coq. Atoms A are parsed as predicates with no args *)

(* ocamllex lexer.mll && menhir parser.mly *)
#use "parser.ml";;
#use "lexer.ml";;

let read_formula s = parse_formula get_token (Lexing.from_string s)
let read_term s = parse_term get_token (Lexing.from_string s)

let f = read_formula "∀x,x=x/\p(x)->∃y,q(x,y,3)"

let _ = (f = read_formula (print_formula f))

let t = read_term "x+3*S(y)"


(** Utilities about formula *)

let rec check_formula sign = function
  | True | False -> true
  | Not f -> check_formula sign f
  | Op(_,f,f') -> check_formula sign f && check_formula sign f'
  | Quant(_,v,f) -> check_formula sign f
  | Pred (p,args) ->
     match List.assoc p sign.pred_symbs with
     | exception Not_found -> false
     | arity when List.length args <> arity -> false
     | _ -> List.for_all (check_term sign) args

let rec allvars = function
  | True | False -> VarSet.empty
  | Not f -> allvars f
  | Op(_,f,f') -> VarSet.union (allvars f) (allvars f')
  | Quant(_,v,f) -> VarSet.add v (allvars f)
  | Pred (p,args) -> termlist_vars args

let rec freevars = function
  | True | False -> VarSet.empty
  | Not f -> freevars f
  | Op(_,f,f') -> VarSet.union (freevars f) (freevars f')
  | Quant(_,v,f) -> VarSet.remove v (freevars f)
  | Pred (p,args) -> termlist_vars args

let rec formula_subst (v,t) f = match f with
  | True | False -> f
  | Pred (p,args) -> Pred (p, List.map (term_subst (v,t)) args)
  | Not f -> Not (formula_subst (v,t) f)
  | Op(o,f,f') -> Op(o, formula_subst (v,t) f, formula_subst (v,t) f')
  | Quant(q,v',_) when v=v' -> f
  | Quant(q,v',f) ->
     let vars_t = term_vars t in
     if not (VarSet.mem v' vars_t) then
       Quant(q,v',formula_subst (v,t) f)
     else
       (* variable capture : we change v' into a fresh variable first *)
       let z = fresh_var (VarSet.union (allvars f) vars_t) in
       let f' = formula_subst (v',Var z) f in
       Quant(q,z,formula_subst (v,t) f')

let rec alpha_equiv f1 f2 = match f1, f2 with
  | True, True -> true
  | False, False -> true
  | Pred (p1,args1), Pred (p2,args2) ->
     (* on terms (and hence term lists) we could use Ocaml's = *)
     p1 = p2 && args1 = args2
  | Not f1, Not f2 -> alpha_equiv f1 f2
  | Op(o1,f1,f1'), Op(o2,f2,f2') when o1=o2 ->
     alpha_equiv f1 f2 && alpha_equiv f1' f2'
  | Quant(q1,v1,f1'), Quant(q2,v2,f2') when q1=q2 ->
     let z = fresh_var (VarSet.union (allvars f1) (allvars f2)) in
     alpha_equiv
       (formula_subst (v1,Var z) f1')
       (formula_subst (v2,Var z) f2')
  | _ -> false

let _ = alpha_equiv (read_formula "∀x,A(x)->A(x)")
                    (read_formula "∀z,A(z)->A(z)")

(** Is there some term [t] such that [term_subst (x,t) u = v] ? *)

type instance_answer =
  | Any
  | Exactly of term
  | No

let merge_two_instances a a' = match a,a' with
  | No,_ | _,No -> No
  | Any,a | a,Any -> a
  | Exactly t, Exactly t' -> if t = t' then Exactly t else No

let rec merge_instances = function
  | [] -> Any
  | a::al -> merge_two_instances a (merge_instances al)

let samelength l l' = (List.length l = List.length l')

(** Is there some term [t] such that
    [term_subst (x,t) u) v] ? *)

let rec term_instanceof x u v = match u with
  | Var y when x=y -> Exactly v
  | Var y -> if u = v then Any else No
  | Fun (f,args) ->
     match v with
     | Fun (f',args') when f=f' && samelength args args' ->
        termlist_instanceof x args args'
     | _ -> No

and termlist_instanceof x ul vl =
  let pbs = List.combine ul vl in
  let sols = List.map (fun (u,v) -> term_instanceof x u v) pbs in
  merge_instances sols

(** Is there some term [t] such that
    [alpha_equiv (formula_subst (x,t) f1) f2] ? *)

let rec instanceof x f1 f2 = match f1,f2 with
  | True,True -> Any
  | False,False -> Any
  | Pred(p,args),Pred(p',args') when p=p' && samelength args args' ->
     termlist_instanceof x args args'
  | Not(f1), Not(f2) -> instanceof x f1 f2
  | Op(o1,f1,f1'), Op(o2,f2,f2') when o1=o2 ->
     merge_two_instances (instanceof x f1 f2) (instanceof x f1' f2')
  | Quant(q1,v1,f1'),Quant(q2,v2,f2') when q1=q2 ->
     let z = fresh_var
               (VarSet.add x (VarSet.union (allvars f1) (allvars f2))) in
     instanceof x
       (formula_subst (v1,Var z) f1')
       (formula_subst (v2,Var z) f2')
  | _ -> No

(** Contexts *)

type context = formula list

let print_ctx c = String.concat "," (List.map print_formula c)

let check_ctx sign c = List.for_all (check_formula sign) c

let rec allvars_ctx = function
  | [] -> VarSet.empty
  | f::c -> VarSet.union (allvars f) (allvars_ctx c)

let rec freevars_ctx = function
  | [] -> VarSet.empty
  | f::c -> VarSet.union (freevars f) (freevars_ctx c)

let ctx_subst (v,t) c = List.map (formula_subst (v,t)) c


(** Sequent *)

type sequent = context * formula  (** hypotheses, conclusion *)

let print_seq (ctx,concl) = print_ctx ctx ^ " |- " ^ print_formula concl

let check_seq sign (ctx,concl) =
  check_ctx sign ctx && check_formula sign concl

let allvars_seq (ctx,concl) = VarSet.union (allvars_ctx ctx) (allvars concl)

let freevars_seq (ctx,concl) = VarSet.union (freevars_ctx ctx) (freevars concl)

let seq_subst (v,t) (ctx,concl) =
  (ctx_subst (v,t) ctx, formula_subst (v,t) concl)


(** Derivation *)

type rule_name =
  | Alpha
  | Ax
  | True_i
  | False_e
  | Not_i | Not_e
  | And_i | And_e1 | And_e2
  | Or_i1 | Or_i2 | Or_e
  | Impl_i | Impl_e
  | All_i | All_e
  | Ex_i | Ex_e
  | Absurd

type derivation =
  | Rule of rule_name * sequent * derivation list

let dseq = function Rule (_,s,_) -> s

let valid_deriv_step ?(classic=true) = function
  | Rule (Alpha,(g,a),[Rule(_,(g',a'),_)]) -> g=g' && alpha_equiv a a'
  | Rule (Ax,(g,a),[]) -> List.mem a g
  | Rule (True_i,(_,True),[]) -> true
  | Rule (False_e,(g,_),[d]) -> dseq d = (g,False)
  | Rule (Not_i,(g,Not(a)),[d]) -> dseq d = (a::g,False)
  | Rule (Not_e,(g,False),[Rule(_,(g1,a),_);Rule(_,(g2,Not(a')),_)]) ->
     a=a' && g=g1 && g=g2
  | Rule (And_i,(g,Op(And,a,b)),[d1;d2]) -> dseq d1 = (g,a) && dseq d2 = (g,b)
  | Rule (And_e1,seq,[Rule(_,(g,Op(And,a,_)),_)]) -> seq = (g,a)
  | Rule (And_e2,seq,[Rule(_,(g,Op(And,_,b)),_)]) -> seq = (g,b)
  | Rule (Or_i1,(g,Op(Or,a,_)),[d]) -> dseq d = (g,a)
  | Rule (Or_i2,(g,Op(Or,_,b)),[d]) -> dseq d = (g,b)
  | Rule (Or_e,(g,c),[Rule(_,(g',Op(Or,a,b)),_);d2;d3]) ->
     g=g' && dseq d2 = (a::g,c) && dseq d3 = (b::g,c)
  | Rule(Impl_i,(g,Op(Impl,a,b)),[d]) -> dseq d = (a::g, b)
  | Rule(Impl_e,seq,[Rule(_,(g,Op(Impl,a,b)),_);d2]) ->
     seq = (g,b) && dseq d2 = (g,a)
  | Rule(All_i,(g,Quant(All,x,a)),[d]) ->
     dseq d = (g,a) && not (VarSet.mem x (freevars_ctx g))
  | Rule(All_e,(g,b),[Rule(_,(g',Quant(All,x,a)),_)]) ->
     g=g' && instanceof x a b <> No
  | Rule(Ex_i,(g,Quant(Ex,x,a)),[Rule(_,(g',b),_)]) ->
     g=g' && instanceof x a b <> No
  | Rule(Ex_e,(g,b),[Rule(_,(g',Quant(Ex,x,a)),_);d2]) ->
     g=g' && dseq d2 = (a::g,b) &&
     not (VarSet.mem x (freevars_ctx g)) &&
     not (VarSet.mem x (freevars b))
  | Rule(Absurd,seq,[Rule(_,(Not(a)::g,False),_)]) when classic -> seq = (g,a)
  | _ -> false

let rec valid_deriv ?(classic=true) d = match d with
  | Rule(_,_,ld) ->
     valid_deriv_step ~classic d &&
       List.for_all (valid_deriv ~classic) ld

let example =
  let fm = read_formula in
  Rule(Impl_i,([],fm"A->A"),
       [Rule(Ax,([fm"A"],fm"A"),[])])

let _ = valid_deriv ~classic:false example

let mkrule r ctx concl ds =
  let fm = read_formula in
  Rule(r,(List.map fm ctx, fm concl), ds)

let example' =
  mkrule Impl_i [] "A->A"
    [mkrule Ax ["A"] "A" []]

let _ = (example = example')

let example2 =
  mkrule Impl_i [] "{∀x,A(x)/\B(x)}->{∀x,A(x)}/\{∀x,B(x)}"
    (let c = "∀x,A(x)/\B(x)" in
     [mkrule And_i [c] "{∀x,A(x)}/\{∀x,B(x)}"
       [mkrule All_i [c] "∀x,A(x)"
         [mkrule And_e1 [c] "A(x)"
           [mkrule All_e [c] "A(x)/\B(x)"
             [mkrule Ax [c] c []]]]
        ;
        mkrule All_i [c] "∀x,B(x)"
         [mkrule And_e2 [c] "B(x)"
           [mkrule All_e [c] "A(x)/\B(x)"
             [mkrule Ax [c] c []]]]]])

let _ = valid_deriv ~classic:false example2

let em =
  mkrule Absurd [] "A\/~A"
    [mkrule Not_e ["~{A\/~A}"] "False"
       [mkrule Or_i2  ["~{A\/~A}"] "A\/~A"
         [mkrule Not_i ["~{A\/~A}"] "~A"
           [mkrule Not_e ["A";"~{A\/~A}"] "False"
             [mkrule Or_i1 ["A";"~{A\/~A}"] "A\/~A"
               [mkrule Ax ["A";"~{A\/~A}"] "A" []]
              ;
              mkrule Ax ["A";"~{A\/~A}"] "~{A\/~A}" []]]]
        ;
        mkrule Ax ["~{A\/~A}"] "~{A\/~A}" []]]

let _ = valid_deriv ~classic:true em
let _ = valid_deriv ~classic:false em

(** Example of free alpha-renaming during a proof,
    (not provable without the Alpha rule) *)

let captcha =
  mkrule Alpha ["A(x)"] "∀x,A(x)->A(x)"
   [mkrule All_i ["A(x)"] "∀z,A(z)->A(z)"
    [mkrule Impl_i ["A(x)"] "A(z)->A(z)"
     [mkrule Ax ["A(z)";"A(x)"] "A(z)" []]]]

let _ = valid_deriv ~classic:false captcha

let captcha_bug =
  mkrule All_i ["A(x)"] "∀x,A(x)->A(x)"
   [mkrule Impl_i ["A(x)"] "A(x)->A(x)"
    [mkrule Ax ["A(x)";"A(x)"] "A(x)" []]]

let _ = valid_deriv ~classic:false captcha_bug
